#!/usr/bin/env python 
"""
Polar Move - (copy and) move selected objects by distance and angle

Copyright (C) 2012 ~suv, suv-sf@users.sourceforge.net

Idea:
> Topic: Move objects by distance and angle
> 
> I guess that subject is obvious so I don't dig any deeper.
> 
> Is there a method in Inkscape that is more effective then doing the
> math externally?
<http://www.inkscapeforum.com/viewtopic.php?f=5&t=12505>


This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# standard library
from copy import deepcopy

# local library
import inkex
import simpletransform

try:
    inkex.localize()
except:
    import gettext
    _ = gettext.gettext

class MovePolar(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("--movemode",
            action="store", type="string",
            dest="movemode", default="copy",
            help="Move original, a copy or a clone")
        self.OptionParser.add_option("--unit",
            action="store", type="string",
            dest="unit", default="px",
            help="The unit of the measurement")
        self.OptionParser.add_option("--distance",
            action="store", type="float",
            dest="distance", default=100.0,
            help="Distance to move selection")
        self.OptionParser.add_option("--angle",
            action="store", type="float",
            dest="angle", default=30.0,
            help="Angle for polar move")
        self.OptionParser.add_option("--direction",
            action="store", type="string",
            dest="direction", default="ccw",
            help="Direction (CW or CCW)")
        self.OptionParser.add_option("--wrapped",
            action="store", type="inkbool",
            dest="wrapped", default=False,
            help="Transform group wrapping object")
        self.OptionParser.add_option("--tab",
            action="store", type="string",
            dest="tab")

    def polar_arrange(self, node, length, angle):
        '''
        Takes object, distance and angle and moves, copies or clones it to new location
        '''
        # store parent, index of original object
        nodeParent     = node.getparent()
        nodeIndex      = nodeParent.index(node)
        newNodeIndex   = nodeIndex+1

        # prepare to move original, clone or copy
        if self.options.movemode == "move":
            newNode = node
        elif self.options.movemode == "copy":
            newNode = deepcopy(node)
            newId = node.get('id') + "-1"
            newNode.set('id', newId)
        elif self.options.movemode == "clone":
            newNode = inkex.etree.Element(inkex.addNS('use', 'svg'))
            newNode.set(inkex.addNS('href', 'xlink'),"#%s" % node.get('id'))
        else:
            inkex.debug("Unsupported move mode.")
            return

        # direction (in SVG coords, + is cw, - is ccw)
        if self.options.direction == "ccw":
            angle = -(angle)

        # convert units to px (SVG user units)
        length_uu = self.unittouu(str(length) + self.options.unit)

        # transforms
        tr_rotate     = 'rotate(%s)' % angle
        tr_translate  = 'translate(%s)' % length_uu
        tr_rotate_rev = 'rotate(%s)' % -(angle)

        tr_concat = " ".join([tr_rotate, tr_translate, tr_rotate_rev])

        # wrap, transform, move/append
        if self.options.wrapped:
            # put newNode in group, set concat transform on group
            newNodeGroup = inkex.etree.Element(inkex.addNS('g', 'svg'))
            newNodeGroup.set('transform', tr_concat)
            if self.options.movemode == "move":
                nodeParent.remove(newNode)
                newNodeIndex = nodeIndex
            nodeParent.insert(newNodeIndex, newNodeGroup)
            newNodeGroup.append(newNode)
        elif self.options.movemode == "clone":
            # create clone, set concatenated transform on clone
            newNode.set('transform', tr_concat)
            nodeParent.insert(newNodeIndex, newNode)
        else:
            # apply transformation matrix to newNode
            mat = simpletransform.parseTransform(tr_concat)
            simpletransform.applyTransformToNode(mat, newNode)
            if self.options.movemode != "move":
                nodeParent.insert(newNodeIndex, newNode)

    def effect(self):
        '''
        Effect: loop through selection and arrange each child individually
        '''
        if not hasattr(self, 'unittouu'):
            self.unittouu = inkex.unittouu
        # transform each selected object individually
        for node in self.selected.itervalues():
            self.polar_arrange(node, self.options.distance, self.options.angle)

if __name__ == '__main__':
    e = MovePolar()
    e.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
